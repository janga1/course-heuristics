public class Field {

		private Tile[][] field;
		private int width, height;

		public Field(int w, int h){
				width = w;
				height = h;
				field = new Tile[w][h];
		}

		public boolean doesTileFit(Tile tile, int xStart, int yStart) {
				if (width - xStart >= tile.getWidth() && height - yStart >= tile.getHeight()) {
						boolean placed = true;

						for (int i = xStart ; i < (xStart + tile.getWidth()) ; i++) {
								for (int j = yStart ; j < (yStart + tile.getHeight()) ; j++) {
										if (field[i][j] != null) {
												placed = false;
										}
								}
						}

						if (placed) {
								for (int i = xStart ; i < (xStart + tile.getWidth()) ; i++) {
										for (int j = yStart ; j < (yStart + tile.getHeight()) ; j++) {
												field[i][j] = tile;
										}
								}
						}
						return placed;
				}
				return false;
		}

		public boolean placeTile(Tile tile, int xStart, int yStart){
				if (width - xStart >= tile.getWidth() && height - yStart >= tile.getHeight()) {
						for (int i = xStart ; i < (xStart + tile.getWidth()) ; i++) {
								for (int j = yStart ; j < (yStart + tile.getHeight()) ; j++) {
										field[i][j] = tile;
								}
						}
						return true;
				}
				return false;
		}

		public void removeTile(Tile tile) {
				for (int i = tile.getXCoord() ; i < (tile.getXCoord() + tile.getWidth()) ; i++) {
						for (int j = tile.getYCoord() ; j < (tile.getYCoord() + tile.getHeight()) ; j++) {
								if (i < width && j < height) {
										field[i][j] = null;
								}
						}
				}
		}

}
