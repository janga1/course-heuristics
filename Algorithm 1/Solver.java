import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import javax.imageio.ImageIO;
import java.awt.Color;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.util.Date;


public class Solver {

    private static final String PATTERN  = "\\d+";
    private static final String CHARSET  = "ISO-8859-1";
    private static final int CORRECTION  = 21;
    private static final int BORDER_SIZE = 40;
    private static final Color GREYISH   = new Color(190, 190, 190);

    private static final long serialVersionUID = -360885512080963508L;

    private Field field;
    private int fieldWidth = 0,
                fieldHeight = 0,
                fieldScale = 0;
    private List<Tile> placedTiles = new ArrayList<Tile>();
    private String fileName = "";

    // CHECK IF ARGUMENT IS GIVEN BEFORE START OF PROGRAM
    public static void main(String[] args) {
        if (args.length != 1) {
            System.out.println("Invalid arguments given.");
            System.exit(1);
        }

        Solver solver = new Solver();
        solver.start(args[0]);
    }

    // START OF THE PROGRAM
    public void start(String file) {
        System.out.println(new Date());

        fileName = file;
        Path path = Paths.get(file);
        Charset charset = Charset.forName(CHARSET);

        try {
            List<Tile> tileSet = parseFile(Files.readAllLines(path, charset));
            List<Tile> sortedTileSet = sortTiles(tileSet);
            solveField(sortedTileSet);
            paintBoard();
        } catch (IOException e) {
            System.out.println(e);
        }

        System.out.println(new Date());
    }

    // PARSE THE FILE
    private List<Tile> parseFile(List<String> lines) {
        String headerLine = lines.get(0);
        Pattern r = Pattern.compile(PATTERN);
        Matcher m = r.matcher(headerLine);

        List<Tile> tileSet = new ArrayList<Tile>();

        m.find();
        fieldWidth = Integer.parseInt(m.group(0));
        m.find();
        fieldHeight = Integer.parseInt(m.group(0));
        m.find();
        fieldScale = Integer.parseInt(m.group(0));

        field = new Field(fieldWidth, fieldHeight);

        for (int line = 1 ; line < lines.size() ; line++) {
            m = r.matcher(lines.get(line));

            m.find();
            int numberOfTiles = Integer.parseInt(m.group(0));
            m.find();
            int tileWidth = Integer.parseInt(m.group(0));
            m.find();
            int tileHeight = Integer.parseInt(m.group(0));

            for (int count = 0 ; count < numberOfTiles ; count++) {
                Tile tile = new Tile();
                tile.setWidth(tileWidth);
                tile.setHeight(tileHeight);

                tileSet.add(tile);
            }
        }
        return tileSet;
    }

    // SORT THE TILES FROM BIGGEST TO SMALLEST AREA
    public List<Tile> sortTiles(List<Tile> tileSet) {
        Collections.sort(tileSet, new Tile()); //Sort smallest to biggest area
        Collections.reverse(tileSet); // turn around
        return tileSet;
    }

    // SOLVE THE FIELD
    private void solveField(List<Tile> tileSet) {
        boolean filled = fill(0, 0, fieldWidth, fieldHeight, tileSet);
    }

    private boolean fill(int xStart, int yStart, int xEnd, int yEnd, List<Tile> tiles) {
        int width = xEnd - xStart;
        int height = yEnd - yStart;
        int currentSize = tiles.size();

        if (width == 0 || height == 0) {
            return true;
        }

        Tile tile;

        for (int i = 0 ; i < tiles.size() ; i++) {
            tile = tiles.remove(i);
            if (tile.getWidth() == width && tile.getHeight() == height) {
                if (field.doesTileFit(tile, xStart, yStart)) {
                    tile.setXCoord(xStart);
                    tile.setYCoord(yStart);
                    placedTiles.add(0, tile);
                    boolean filled = fill(xStart, yStart + tile.getHeight(), xEnd, yEnd, tiles);
                    if (filled) {
                        return true;
                    } else {
                        int tilesPlaced = currentSize - tiles.size();
                        for (int j = 0 ; j < tilesPlaced ; j++) {
                            Tile removedTile = placedTiles.remove(0);
                            field.removeTile(removedTile);
                            tiles.add(removedTile);
                        }
                    }
                }
            } else if (tile.getHeight() == width && tile.getWidth() == height) {
                tile.flip();

                if (field.doesTileFit(tile, xStart, yStart)) {
                    tile.setXCoord(xStart);
                    tile.setYCoord(yStart);
                    placedTiles.add(0, tile);
                    boolean filled = fill(xStart, yStart + tile.getHeight(), xEnd, yEnd, tiles);
                    if (filled) {
                        return true;
                    } else {
                        int tilesPlaced = currentSize - tiles.size();
                        for (int j = 0 ; j < tilesPlaced ; j++) {
                            Tile removedTile = placedTiles.remove(0);
                            field.removeTile(removedTile);
                            tiles.add(removedTile);
                        }
                    }
                }
            }
            tiles.add(i, tile);
        }

        for (int i = 0 ; i < tiles.size() ; i++) {
            tile = tiles.remove(i);
            if (tile.getWidth() == width && tile.getHeight() <= height) {
                if (field.doesTileFit(tile, xStart, yStart)) {
                    tile.setXCoord(xStart);
                    tile.setYCoord(yStart);
                    placedTiles.add(0, tile);
                    boolean filled = fill(xStart, yStart + tile.getHeight(), xEnd, yEnd, tiles);
                    if (filled) {
                        return true;
                    } else {
                        int tilesPlaced = currentSize - tiles.size();
                        for (int j = 0 ; j < tilesPlaced ; j++) {
                            Tile removedTile = placedTiles.remove(0);
                            field.removeTile(removedTile);
                            tiles.add(removedTile);
                        }
                    }
                }
            } else if (tile.getHeight() == width && tile.getWidth() <= height) {
                tile.flip();
                if (field.doesTileFit(tile, xStart, yStart)) {
                    tile.setXCoord(xStart);
                    tile.setYCoord(yStart);
                    placedTiles.add(0, tile);
                    boolean filled = fill(xStart, yStart + tile.getHeight(), xEnd, yEnd, tiles);
                    if (filled) {
                        return true;
                    } else {
                        int tilesPlaced = currentSize - tiles.size();
                        for (int j = 0 ; j < tilesPlaced ; j++) {
                            Tile removedTile = placedTiles.remove(0);
                            field.removeTile(removedTile);
                            tiles.add(removedTile);
                        }
                    }
                }
            } else if (tile.getHeight() == height && tile.getWidth() <= width) {
                if (field.doesTileFit(tile, xStart, yStart)) {
                    tile.setXCoord(xStart);
                    tile.setYCoord(yStart);
                    placedTiles.add(0, tile);
                    boolean filled = fill(xStart + tile.getWidth(), yStart, xEnd, yEnd, tiles);
                    if (filled) {
                        return true;
                    } else {
                        int tilesPlaced = currentSize - tiles.size();
                        for (int j = 0 ; j < tilesPlaced ; j++) {
                            Tile removedTile = placedTiles.remove(0);
                            field.removeTile(removedTile);
                            tiles.add(removedTile);
                        }
                    }
                }
            } else if (tile.getWidth() == height && tile.getHeight() <= width) {
                tile.setXCoord(xStart);
                tile.setYCoord(yStart);
                tile.flip();
                if (field.doesTileFit(tile, xStart, yStart)) {
                    tile.setXCoord(xStart);
                    tile.setYCoord(yStart);
                    placedTiles.add(0, tile);
                    boolean filled = fill (xStart + tile.getWidth(), yStart, xEnd, yEnd, tiles);
                    if (filled) {
                        return true;
                    } else {
                        int tilesPlaced = currentSize - tiles.size();
                        for (int j = 0 ; j < tilesPlaced ; j++) {
                            Tile removedTile = placedTiles.remove(0);
                            field.removeTile(removedTile);
                            tiles.add(removedTile);
                        }
                    }
                }
            } else {
                tiles.add(i, tile);
            }
        }

        for (int i = 0 ; i < tiles.size() ; i++) {
            tile = tiles.remove(i);
            if (tile.getWidth() <= width && tile.getHeight() <= height) {
                if (field.doesTileFit(tile, xStart, yStart)) {
                    tile.setXCoord(xStart);
                    tile.setYCoord(yStart);
                    placedTiles.add(0, tile);

                    int areaUnder = tile.getWidth() * (yEnd - yStart - tile.getHeight());
                    int areaRight = tile.getHeight() * (xEnd - xStart - tile.getWidth());

                    if (areaUnder <= areaRight) {
                        boolean underAreaFilled = fill(xStart, yStart + tile.getHeight(), xStart + tile.getWidth(), yEnd, tiles);
                        if (underAreaFilled) {
                            boolean restAreaFilled = fill(xStart + tile.getWidth(), yStart, xEnd, yEnd, tiles);
                            if (restAreaFilled) {
                                return true;
                            } else {
                                int tilesPlaced = currentSize - tiles.size();
                                for (int j = 0 ; j < tilesPlaced ; j++) {
                                    Tile removedTile = placedTiles.remove(0);
                                    field.removeTile(removedTile);
                                    tiles.add(removedTile);
                                }
                            }
            						} else {
                      			int tilesPlaced = currentSize - tiles.size();
              							for (int j = 0 ; j < tilesPlaced ; j++) {
                                Tile removedTile = placedTiles.remove(0);
                                field.removeTile(removedTile);
                                tiles.add(removedTile);
              							}
            						}
                    } else {
                        boolean rightAreaFilled = fill(xStart+tile.getWidth(), yStart, xEnd, yStart+tile.getHeight(), tiles);
                        if (rightAreaFilled) {
                            boolean restAreaFilled = fill(xStart, yStart+tile.getHeight(), xEnd, yEnd, tiles);
                            if(restAreaFilled){
                                return true;
                            } else {
                                int tilesPlaced = currentSize - tiles.size();
                                for (int j = 0 ; j < tilesPlaced ; j++) {
                                    Tile removedTile = placedTiles.remove(0);
                                    field.removeTile(removedTile);
                                    tiles.add(removedTile);
                                }
                            }
            						} else {
              							int tilesPlaced = currentSize - tiles.size();
              							for (int j = 0 ; j < tilesPlaced ; j++) {
                                Tile removedTile = placedTiles.remove(0);
                                field.removeTile(removedTile);
                                tiles.add(removedTile);
              							}
            						}
          					}
        				}
            } else if (tile.getWidth() <= width && tile.getHeight() <= height) {
        				tile.flip();
        				if (field.doesTileFit(tile, xStart, yStart)) {
          					tile.setXCoord(xStart);
                    tile.setYCoord(yStart);
          					placedTiles.add(0, tile);

          					int areaUnder = tile.getWidth() * (yEnd-yStart-tile.getHeight());
          					int areaRight = tile.getHeight() * (xEnd-xStart-tile.getWidth());

          					if (areaUnder <= areaRight) {
            						boolean underAreaFilled = fill(xStart, yStart+tile.getHeight(), xStart+tile.getWidth(), yEnd, tiles);
            						if (underAreaFilled) {
              							boolean restAreaFilled = fill(xStart+tile.getWidth(), yStart, xEnd, yEnd, tiles);
              							if (restAreaFilled) {
                								return true;
              							} else {
                								int tilesPlaced = currentSize - tiles.size();
                								for (int j = 0 ; j < tilesPlaced ; j++) {
                                    Tile removedTile = placedTiles.remove(0);
                                    field.removeTile(removedTile);
                                    tiles.add(removedTile);
                								}
              							}
                  			} else {
              							int tilesPlaced = currentSize - tiles.size();
              							for (int j = 0 ; j < tilesPlaced ; j++) {
                                Tile removedTile = placedTiles.remove(0);
                                field.removeTile(removedTile);
                                tiles.add(removedTile);
              							}
            						}
          					 } else {
            						boolean rightAreaFilled = fill(xStart+tile.getWidth(), yStart, xEnd, yStart+tile.getHeight(), tiles);
            						if(rightAreaFilled){
              							boolean restAreaFilled = fill(xStart, yStart+tile.getHeight(), xEnd, yEnd, tiles);
              							if(restAreaFilled){
                								return true;
                						} else {
                								int tilesPlaced = currentSize - tiles.size();
                								for (int j = 0 ; j < tilesPlaced ; j++) {
                                    Tile removedTile = placedTiles.remove(0);
                                    field.removeTile(removedTile);
                                    tiles.add(removedTile);
                      					}
                    				}
            						} else {
              							int tilesPlaced = currentSize - tiles.size();
              							for (int j = 0 ; j < tilesPlaced ; j++) {
                                Tile removedTile = placedTiles.remove(0);
                                field.removeTile(removedTile);
                                tiles.add(removedTile);
              							}
            						}
          					}
        				}
      			} else {
        				tiles.add(i, tile);
            }
        }
    		return false;
    }

    // PAINT THE RESULT ON THE SCREEN
    private void paintBoard() {
        String title = fileName.toUpperCase() + ": " + fieldWidth + "X" + fieldHeight + ", " + placedTiles.size() + " TILES";
        JFrame f = new JFrame(title) {
            public void paint(Graphics g) {
                drawBackground(g);
                for (int count = 0 ; count < placedTiles.size() ; count++) {
                    drawTile(g, count);
                    drawGrid(g);
                }
            }
        };

        f.setSize((fieldWidth*fieldScale) + 2 * BORDER_SIZE,
                  (fieldHeight*fieldScale) + CORRECTION + 2 * BORDER_SIZE);
        f.setResizable(true);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.setVisible(true);

        try {
                BufferedImage image = new BufferedImage(f.getWidth(), f.getHeight(), BufferedImage.TYPE_INT_RGB);
                Graphics2D graphics2D = image.createGraphics();
                f.paint(graphics2D);
                ImageIO.write(image, "png", new File(String.format("%s.PNG", fileName)));
            } catch(Exception e) {
                  //code
        }
    }

    private void drawBackground(Graphics g) {
        g.setColor(new Color(255,255,255));
        g.fillRect(0,
                   0,
                  fieldWidth*fieldScale + 2*BORDER_SIZE,
                  fieldHeight*fieldScale + 2*BORDER_SIZE + CORRECTION );
    }

    private void drawTile(Graphics g, int count) {
        g.setColor(new Color((int)(Math.random() * 0x1000000)));
        Tile tile = placedTiles.get(count);
        g.fillRect(tile.getXCoord()*fieldScale + BORDER_SIZE,
                  tile.getYCoord()*fieldScale + BORDER_SIZE + CORRECTION,
                  tile.getWidth()*fieldScale,
                  tile.getHeight()*fieldScale);
    }

    private void drawGrid(Graphics g) {
        g.setColor(GREYISH);
        for (int vert = 0 ; vert <= fieldWidth ; vert++) {
            g.drawLine(vert*fieldScale + BORDER_SIZE,
                      0 + CORRECTION + BORDER_SIZE,
                      vert*fieldScale + BORDER_SIZE,
                      fieldHeight*fieldScale + CORRECTION + BORDER_SIZE);
        }

        for (int hor = 0 ; hor <= fieldHeight ; hor++) {
            g.drawLine(0 + BORDER_SIZE,
                      hor*fieldScale + CORRECTION + BORDER_SIZE,
                      fieldWidth*fieldScale + BORDER_SIZE,
                      hor*fieldScale + CORRECTION + BORDER_SIZE);
        }
    }

}
