import sys, os, re, random
from collections import defaultdict
from prettytable import PrettyTable
from datetime import datetime, timedelta
from PIL import Image, ImageDraw


fieldWidth = fieldHeight = fieldScale = 0
tiles = defaultdict(list)
solutions = list()
lastFound = datetime.now() + timedelta(days=100)

# MAIN
def main():
    print('STARTED SEARCHING {}'.format(datetime.now()))
    parse()
    solve()
    print('ENDED SEARCHING {}'.format(datetime.now()))

# PARSES THE .tiles FILE AND STORES ALL THE INFO
def parse():
    global fieldWidth, fieldHeight, fieldScale, tiles

    with open(str(sys.argv[1])) as f:
        tileNumber = 1
        for line in f:
            result = re.findall(r"\d+", line)
            if "width" in line:
                fieldWidth  = int(result[0])
                fieldHeight = int(result[1])
                fieldScale  = int(result[2])
            else:
                for i in range(int(result[0])):
                    tile = [0,
                            0,
                            int(result[1]),
                            int(result[2]),
                            '#'+''.join([random.choice('89ABCDEF') for x in range(6)])]
                    tiles[tileNumber] = tile
                    tileNumber = tileNumber + 1

# CREATES EMPTY MATRIX AND USEDTILES LIST AND RUNS DFS
def solve():
    matrix = [[0 for x in range(fieldWidth)] for y in range(fieldHeight)]
    usedTiles = list()
    dfs(usedTiles, matrix)


# RECURSIVE DFS ALGO TO PLACE ALL TILES AND RETRIEVE SOLUTION FOR TILESET
def dfs(usedTiles, matrix):
    global lastFound

    previousCategories = list()

    # CHECK IF TO MUCH TIME HAS PASSED SINCE LAST FOUND SOLUTION
    if len(sys.argv) == 3 and datetime.now() - timedelta(minutes=float(sys.argv[2])) > lastFound:
        if input("Sure you want to stop searching? [Y/N]") == "Y":
            print('STOPPED SEARCHING {}'.format(datetime.now()))
            sys.exit()
        else:
            print('CONTINUED SEARCHING {}'.format(datetime.now()))
            lastFound = lastFound + timedelta(minutes=60)

    #print(usedTiles)
    # CHECK IF FILLED FIELD HAS UNIQUE SOLUTION
    if len(usedTiles) == len(tiles):
        if unique(matrix):
            solutionNumber = str(int(len(solutions)/8))
            lastFound = datetime.now()
            message = 'SOLUTION {} FOUND {}'.format(solutionNumber, lastFound)
            print(message)
            saveImage(matrix, solutionNumber, lastFound)
        return

    # GO TROUGH ALL TILES ON EACH LEVEL
    for tile in range(1, len(tiles)+1):
        cat = (tiles[tile][2],tiles[tile][3])
        if str(tile) not in usedTiles and str(tile)+'*' not in usedTiles and cat not in previousCategories:
            previousCategories.append(cat)
            if fits(tile, matrix):
                matrix = add(tile, matrix)
                usedTiles.append(str(tile))
                dfs(usedTiles, matrix)
                usedTiles.remove(str(tile))
                matrix = remove(tile, matrix)
            if not tiles[tile][2] == tiles[tile][3]:
                flip(tile)
                if fits(tile, matrix):
                    matrix = add(tile, matrix)
                    usedTiles.append(str(tile)+'*')
                    dfs(usedTiles, matrix)
                    usedTiles.remove(str(tile)+'*')
                    matrix = remove(tile, matrix)
                flip(tile)


# CHECK IF TILE FITS IN FIRST OPEN SPOT IN MATRIX
def fits(tile, matrix):
    x, y = findFirst(0, matrix)
    w = tiles[tile][2]
    h = tiles[tile][3]

    if x + w > fieldHeight or y + h > fieldWidth:
        return False

    for row in range(x, x + w):
        for col in range(y, y + h):
            if matrix[row][col] != 0:
                return False

    return True


# ADD TILE TO MATRIX
def add(tile, matrix):
    x, y = findFirst(0, matrix)
    w = tiles[tile][2]
    h = tiles[tile][3]
    for row in range(x, x + w):
        for col in range(y, y + h):
            matrix[row][col] = tile

    return matrix


# REMOVE TILE FROM MATRIX
def remove(tile, matrix):
    x,y = findFirst(tile, matrix)
    w = tiles[tile][2]
    h = tiles[tile][3]
    for row in range(x,w+x):
        for col in range(y,h+y):
            matrix[row][col] = 0

    return matrix


# FIND FIRST INSTANCE OF VALUE IN MATRIX AND RETURN X AND Y COORD FOR IT
def findFirst(val, matrix):
    found = [(index, row.index(val)) for index, row in enumerate(matrix) if val in row]
    return found[0]


# FLIP TILE
def flip(tile):
    temp = tiles[tile][3]
    tiles[tile][3] = tiles[tile][2]
    tiles[tile][2] = temp


# CHECKS IF MATRIX/SOLUTION IS UNIQUE
def unique(matrix):
    global solutions

    matrix = categorize(matrix)

    allShapes = list()

    a = matrix
    b = matrix = list(list(x) for x in zip(*matrix))[::-1]
    c = matrix = list(list(x) for x in zip(*matrix))[::-1]
    d = matrix = list(list(x) for x in zip(*matrix))[::-1]

    matrix = [ [ row[ i ] for row in matrix ] for i in range( len( matrix[ 0 ] ) ) ]

    e = matrix
    f = matrix = list(list(x) for x in zip(*matrix))[::-1]
    g = matrix = list(list(x) for x in zip(*matrix))[::-1]
    h = matrix = list(list(x) for x in zip(*matrix))[::-1]

    allShapes = [a,b,c,d,e,f,g,h]

    for i in allShapes:
        if i in solutions:
            return False

    for matrix in allShapes:
        solutions.append(matrix)
    return True


# REPAINTS MATRIX WITH UNIQUE CATOGIES (CATEGORY = ALL TILES WITH SAME DIMENSIONS)
def categorize(matrix):
    newMatrix = [["-" for x in range(fieldWidth)] for y in range(fieldHeight)]

    for tile in tiles:
        for row in range(fieldHeight):
            for col in range(fieldWidth):
                if matrix[row][col] == tile:
                    newMatrix[row][col] = (tiles[tile][2], tiles[tile][3])

    return newMatrix

def rotateElements(matrix):
    for row in range(len(matrix)):
        for col in range(len(matrix[0])):
            matrix[row][col] = reversed(matrix[row][col])

    return matrix


# PAINTS MATRIX IN CONSOLE (ONLY USED FOR TESTING)
def paint(matrix):
    p = PrettyTable()
    for row in matrix:
        p.add_row(row)

    print (p.get_string(header=False, border=False))
    print('')


# SAVE PNG OF SOLUTION IN NEWLY CREATED FOLDER NEXT TO TILES FILE WITH SAME NAME
def saveImage(matrix, solutionNumber, lastFound):
    path = os.path.abspath(sys.argv[1])[:-6]

    if not os.path.exists(path):
        os.makedirs(path)

    fs = fieldScale * 2
    offset = 100

    im = Image.new('RGB', ((fieldWidth * fs) + 2 * offset, (fieldHeight * fs) + 2 * offset), 'white')
    draw = ImageDraw.Draw(im)

    for tile in tiles:
        x,y = findFirst(tile, matrix)
        draw.rectangle([y * fs + offset,
                        x * fs + offset,
                        ((y + tiles[tile][3]) * fs) - 1 + offset,
                        ((x + tiles[tile][2]) * fs) - 1 + offset],
                        fill=tiles[tile][4])

    for vert in range(fieldWidth+1):
        draw.line((vert * fs + offset,
                   0 + offset,
                   vert * fs + offset,
                   fieldHeight * fs + offset), fill='lightgrey', width=1)

    for hor in range(fieldHeight+1):
        draw.line((0 + offset,
                   hor * fs + offset,
                   fieldWidth * fs + offset,
                   hor * fs + offset), fill='lightgrey', width=1)

    draw.text( (offset, fieldHeight * fs + offset),
                "SOLUTION " + str(solutionNumber) + " FOR " + os.path.basename(os.path.abspath(sys.argv[1])) + " FOUND ON " + str(lastFound),
                fill="black")

    location = str(path) + '/' + str(solutionNumber) + '.PNG'
    im.save(location)


main()
