# Course: Heuristics
Created at Vrije Universiteit Amsterdam
## Contributors
  - Michel Woo (https://www.github.com/mwa220))
  - Jan Wieringa
## Description
Two implementations to solve the bin-packing problem, one in Java and one in Python. The former tries to find a solution, the later tries to find as many unique solutions as possible.
## Usage
For questions **A**, **B** and **C** use `python solver.py path/file.tiles`.
For question **D** use `java Solver path/file.tiles` for algorithm 1 or `python solver.py path/file.tiles` for algorithm 2.
## Comments
 1. Algorithm 1 implements this algorithm http://blackpawn.com/texts/lightmaps/default.html
 1. Algorithm 2 implements a depth-first search skipping branches that are duplicates and skipping branches for tiles that can not be flipped (because equal width and depth).
 1. Not all puzzles for question **d** can be solved with algorithm 1.
 1. Puzzle **15-4-2** is broken as the area of the tiles combined is smaller than the area of the field.
